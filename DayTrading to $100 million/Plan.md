# Comprehensive ATH Breakout Day Trading Plan

## Timing and Approach
Night Before: Identify potential candidates using your screeners (all-time highs, relative volume, channel up, and price above SMA20).

This gives you time to narrow down a watchlist, check key support/resistance levels, and plan potential entry/exit points.
Pre-Market (Early Morning): Review the pre-market data to see how your watchlist stocks are reacting to overnight events and whether volume conditions align with your 500k relative volume requirement.

During Market Open: Watch for real-time conditions (volume surges or breakouts) that confirm your entry points. Execute your trades if all criteria are met.

## Scanning (Evening Before Trading Day)
Use Finviz with these filters:
- 52-Week High/Low: 5% or more below
- All-Time High/Low: 5% or more below
- Average Volume: Over 500K
- Earnings Report: This Month (Optional)
- Relative Volume: Over 1.5
- Pattern: Channel Up, Trinagle AScending, or Wedge
- 20-Day Simple Moving Average: Price above SMA20

## Pre-Market (7:00-8:30 AM CST)

### 1. Comprehensive Futures and Sector ETF Review (8:00-8:10 AM CST). Identify which sectors are up.

a) Major Index Futures:
   - S&P 500 (ES): /ES
   - Nasdaq 100 (NQ): /NQ
   - Dow Jones (YM): /YM
   - Russell 2000 (RTY): /RTY

b) Sector-Specific Futures and ETFs:

1. Energy:
   - Crude Oil (CL): /CL
   - Natural Gas (NG): /NG
   - Energy Select Sector SPDR (XLE)

2. Materials:
   - Copper (HG): /HG
   - Materials Select Sector SPDR (XLB)

3. Industrials:
   - Industrials Select Sector SPDR (XLI)

4. Consumer Discretionary:
   - E-mini Consumer Discretionary Select Sector Futures (XAY)
   - Consumer Discretionary Select Sector SPDR (XLY)

5. Consumer Staples:
   - E-mini Consumer Staples Select Sector Futures (XAP)
   - Consumer Staples Select Sector SPDR (XLP)

6. Health Care:
   - E-mini Health Care Select Sector Futures (XVV)
   - Health Care Select Sector SPDR (XLV)

7. Financials:
   - E-mini Financial Select Sector Futures (XAF)
   - Financial Select Sector SPDR (XLF)

8. Information Technology:
   - E-mini Technology Select Sector Futures (XAK)
   - Technology Select Sector SPDR (XLK)

9. Communication Services:
   - Communication Services Select Sector SPDR (XLC)

10. Utilities:
    - E-mini Utilities Select Sector Futures (XAU)
    - Utilities Select Sector SPDR (XLU)

11. Real Estate:
    - Real Estate Select Sector SPDR (XLRE)

c) For each future/ETF, note:
   - Direction (up/down)
   - Magnitude of move (% change)
   - Any significant gaps
   - Volume compared to average

d) Assess overall market sentiment and sector strength:
   - Rank sectors by performance
   - Identify top 3 strongest and weakest sectors

### 2. Review Pre-Market Movers and News (8:15-8:25 AM)
   - Check top pre-market gainers/losers in each sector
   - Review any relevant news for watchlist stocks
   - Note stocks with earnings reports today

### 3. Refine Watchlist 
   - Prioritize 3-5 top candidates
   - Focus on stocks in top 3 performing sectors
   - Ensure alignment with overall market sentiment

## Trading Session (8:30 - 10 AM CST)
Entry Criteria:
- Stock breaks above recent high or ATH
- Breakout volume > 50% above 20-day average volume
- Stock is in one of the top-performing sectors
- Aligns with overall market sentiment

Position Management:
- Size: Max $15,000 per trade
- Stop Loss: 1% below entry
- Target: 5% above entry
- Exit: At stop loss, target, or 10:00 AM

## Post-Trading
1. Record trades in journal, including:
   - Entry and exit points
   - Sector performance correlation
   - Alignment with pre-market analysis
2. Update watchlist for next session

## Weekly Review
1. Analyze week's trades
2. Assess accuracy of sector performance in predicting successful trades
3. Adjust strategy if needed
4. Set next week's goals

Remember: Stick to the plan. No impulsive trades. Focus on consistent execution.
