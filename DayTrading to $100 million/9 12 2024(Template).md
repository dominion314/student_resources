## Pre-Market (7:30-8:30 AM CST)
### 1. Comprehensive Futures and Sector ETF Review (8:00-8:10 AM CST). Identify which sectors are up.

a) Major Index Futures/Direction:
   - S&P 500 (SPY):  
   - Nasdaq 100 (NDX):  
   - Dow Jones (DJI):  
   - Russell 2000 (RUT):  

## Based on the leading sector, check specific futures and ETFs. Collect the following data for said industry:

b) Sector-Specific Futures and ETFs:

Utilities, Communication, Consumer Staples

   - Direction (up/down) = 
   - Magnitude of move (% change) = 
   - Any significant gaps =  
   - Volume compared to average =  
   - Biggest premarket gainer and loser for the sector = 

### 2. Choose 2-3 Stocks that meet your Criteria and list the following:
##  
  
  Stock 1 = RIVN
    - 52 week low/high = 
    - Price =  
    - Entry =  
    - Exit  =
    - Entry =  
    - Exit =  
    - Size =  
    - Volume =  
    - Profit =  

    Stock 2 =  
    - 52 week low/high =  
    - Price = 
    - Entry =  
    - Exit =
    - Entry =  
    - Exit =  
    - Size =  
    - Volume =  
    - Profit = 

RECAP: 


Position Management:
- Size: Max $15,000 per trade
- Stop Loss: 2-5% below entry
- Target: 5% above entry
- Exit: At stop loss, target, or 10:00 AM

Remember: Stick to the plan. No impulsive trades. Focus on consistent execution.
