Comprehensive Futures Options Day Trading Plan

##Timing and Approach

Night Before: Identify potential futures candidates using your preferred screeners and analysis tools.
Pre-Market (Early Morning): Review pre-market data to see how your watchlist futures are reacting to overnight events.
During Market Open: Watch for real-time conditions that confirm your entry points. Execute your trades if all criteria are met.

##Scanning (Evening Before Trading Day)

Use your preferred futures screening tool with these considerations:

Average Volume: Over 1,000,000 (ensures liquidity)
Option/Short: Optionable (ensures options are available)
Market Cap: Mid ($2B-$10B) or Large ($10B+) (generally more stable)
Beta: Between 1 and 2 (indicates some volatility, good for options)
Price: Above $10 (many brokers require this for options trading)
Relative Volume: Over 1 (indicates current interest)

Additional filters to consider:

Volatility: High (% volatility week, month, etc.)
Analyst Recommendations: Strong Buy or Buy


##Futures approaching significant price levels (e.g., recent highs, round numbers)

Average Volume: Ensure sufficient liquidity
Volatility: Look for futures with moderate to high implied volatility
Trend: Identify futures in clear uptrends or downtrends
Technical Indicators: Price above/below key moving averages (e.g., 20-day SMA)

##Once you have a list of stocks, research their options chains separately:

Look for options with high open interest and volume
Check for tight bid-ask spreads
Examine implied volatility levels
Consider upcoming events that might affect the stock price

##LAST
For day trading specifically, also consider:

Stocks making significant moves in pre-market or after-hours trading
Stocks with recent news or analyst upgrades/downgrades
Technical setups like breakouts or reversals


Always perform due diligence on the underlying company:

Check recent news and earnings reports
Understand the company's business model and market position
Be aware of any upcoming events that could impact the stock price


Pre-Market (6:00-9:30 AM EST)
1. Comprehensive Futures and Market Review (6:00-6:30 AM EST)
a) Major Index Futures:

S&P 500 (ES): /ES
Nasdaq 100 (NQ): /NQ
Dow Jones (YM): /YM
Russell 2000 (RTY): /RTY

b) Commodity Futures:

Crude Oil (CL): /CL
Natural Gas (NG): /NG
Gold (GC): /GC
Silver (SI): /SI
Copper (HG): /HG

c) Currency Futures:

Euro FX (6E): /6E
Japanese Yen (6J): /6J

d) Interest Rate Futures:

10-Year T-Note (ZN): /ZN
30-Year T-Bond (ZB): /ZB

e) For each future, note:

Direction (up/down)
Magnitude of move (% change)
Any significant gaps
Volume compared to average

f) Assess overall market sentiment:

Rank futures by performance
Identify top 3 strongest and weakest futures

2. Review Pre-Market Movers and News (6:30-7:00 AM EST)

Check top pre-market movers in each futures category
Review any relevant news that might impact futures markets
Note any significant economic reports due today

3. Refine Watchlist (7:00-7:30 AM EST)

Prioritize 3-5 top futures candidates
Focus on futures with clear directional bias
Ensure alignment with overall market sentiment

4. Options Chain Analysis (7:30-8:00 AM EST)

Review options chains for your selected futures
Identify strikes with high open interest and tight bid-ask spreads
Note implied volatility levels

Trading Session (9:30 AM - 4:00 PM EST)
Entry Criteria:

Future breaks above/below significant level (e.g., recent high/low)
Breakout volume > 50% above 20-day average volume
Aligns with overall market sentiment

Options Strategy:

For bullish setups: Buy ATM or slightly OTM call options
For bearish setups: Buy ATM or slightly OTM put options
Choose options with nearest expiration date (considering day trading)

Position Management:

Size: Max $15,000 per trade
Stop Loss: Exit if underlying future moves 1% against your position or if option loses 20% of its value
Target: Exit when underlying future moves 2% in your favor or option gains 50%
Time-Based Exit: Close position by 3:30 PM EST if neither stop loss nor target is hit

Risk Management:

Maximum of 5 trades per day
No more than 40% of total capital at risk at any time
If 2 consecutive losing trades, stop trading for the day

Post-Trading

Record trades in journal, including:

Entry and exit points for both future and option
Greeks at entry (Delta, Theta, Vega)
Profit/Loss
Reason for entry and exit


Update watchlist for next session

Weekly Review

Analyze week's trades
Assess accuracy of entry criteria
Review option selection (strike price, expiration)
Adjust strategy if needed
Set next week's goals

Remember: Futures options trading carries significant risk. Stick to the plan. No impulsive trades. Focus on consistent execution. Consider paper trading to test this strategy before using real capital.